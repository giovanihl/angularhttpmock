import { Injectable, Injector, Optional } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { HttpMockService } from './http-mock.service';
import { Observable, of } from 'rxjs';

@Injectable()
export class HttpMockInterceptorService implements HttpInterceptor {

  constructor(
    private httpMockService: HttpMockService
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    const mockedResponse = this.httpMockService.handle(request);

    if (mockedResponse) {
      console.log('Loading from HttpMock: ' + request.urlWithParams, { req: request, res: mockedResponse });
      return of(mockedResponse);
    }

    return next.handle(request);
  }
}
